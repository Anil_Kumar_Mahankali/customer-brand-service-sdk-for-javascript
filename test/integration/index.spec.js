import CustomerBrandServiceSdk from '../../src/index';
import config from './config';
import factory from './factory';
import jwt from 'jwt-simple';
import dummy from '../dummy';
import CustomerBrandView from '../../src/customerBrandView';

describe('Index module', () => {

    describe('default export', () => {
        it('should be CustomerBrandServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new CustomerBrandServiceSdk(config.customerBrandServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(CustomerBrandServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('getCustomerBrandWithId method', () => {
            it('should return CustomerBrandView', (done) =>{

                /**
                 * arrange
                 */
                const objectUnderTest =
                    new CustomerBrandServiceSdk(config.customerBrandServiceSdkConfig);

                /**
                 * act
                 */
                const getCustomerBrandWithIdPromise =
                objectUnderTest.getCustomerBrandWithId(
                    1,
                    factory.constructValidPartnerRepOAuth2AccessToken()
                )

                /**
                 * assert
                 */
                getCustomerBrandWithIdPromise
                    .then((CustomerBrandView)=> {
                        expect(CustomerBrandView).toBeTruthy();
                        done();
                        }
                    ).catch(error=> done.fail(JSON.stringify(error)));
            });
        });

        describe('listLevel1CustomerBrands method', () => {
            it('should return more than 1 result', (done) => {


                /*
                 arrange
                 */
                const objectUnderTest =
                    new CustomerBrandServiceSdk(config.customerBrandServiceSdkConfig);

                /*
                 act
                 */
                const level1CustomerBrandsWithSegmentIdPromise =
                    objectUnderTest.listLevel1CustomerBrandsWithSegmentId(
                        8,
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 assert
                 */
                level1CustomerBrandsWithSegmentIdPromise
                    .then((level1CustomerBrands) => {
                        expect(level1CustomerBrands.length>=0).toBeTruthy()
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            })
        });

        describe('listLevel2CustomerBrands method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new CustomerBrandServiceSdk(config.customerBrandServiceSdkConfig);

                /*
                 act
                 */
                const level2CustomerBrandsWithBrandIdPromise =
                    objectUnderTest.listLevel2CustomerBrandsWithBrandId(
                        10,22,
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 assert
                 */
                level2CustomerBrandsWithBrandIdPromise
                    .then((level2CustomerBrandView) => {
                        expect(level2CustomerBrandView.length).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            })
        });

    });
});