import Level1CustomerBrandView from '../../src/level1CustomerBrandView';
import dummy from '../dummy';

describe('Level1CustomerBrandView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new Level1CustomerBrandView(null, dummy.customerBrandName, dummy.customerSegmentId);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = 1;

            /*
             act
             */
            const objectUnderTest =
                new Level1CustomerBrandView(expectedId, dummy.customerBrandName, dummy.customerSegmentId);

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new Level1CustomerBrandView(dummy.customerBrandId, null, dummy.customerSegmentId);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });

        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.customerBrandName;

            /*
             act
             */
            const objectUnderTest =
                new Level1CustomerBrandView(dummy.customerBrandId, expectedName, dummy.customerSegmentId);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });

        it('throws if customerSegmentId is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new Level1CustomerBrandView(dummy.customerBrandId, dummy.customerBrandName, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'customerSegmentId required');
        });

        it('sets customerSegmentId', () => {
            /*
             arrange
             */
            const expectedCustomerSegmentId = 1;

            /*
             act
             */
            const objectUnderTest =
                new Level1CustomerBrandView(dummy.customerBrandId, dummy.customerBrandName, expectedCustomerSegmentId);

            /*
             assert
             */
            const actualCustomerSegmentId = objectUnderTest.customerSegmentId;
            expect(actualCustomerSegmentId).toEqual(expectedCustomerSegmentId);

        });
    })
});
