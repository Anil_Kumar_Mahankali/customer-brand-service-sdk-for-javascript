import {inject} from 'aurelia-dependency-injection';
import CustomerBrandServiceSdkConfig from './customerBrandServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import CustomerBrandView from './customerBrandView';
import CustomerBrandViewFactory from './customerBrandViewFactory';

@inject(CustomerBrandServiceSdkConfig, HttpClient)
class GetCustomerBrandWithIdFeature {

    _config:CustomerBrandServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:CustomerBrandServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     *
     * @param customerBrandWithId
     * @param accessToken
     * @returns {Promise<CustomerBrandView>}
     */
    execute(customerBrandWithId:number,accessToken:string):Promise<CustomerBrandView> {

            return this._httpClient
                .createRequest(`/customer-brands/${customerBrandWithId}`)
                .asGet()
                .withBaseUrl(this._config.precorConnectApiBaseUrl)
                .withHeader('Authorization', `Bearer ${accessToken}`)
                .send()
                .then(
                    (response) => CustomerBrandViewFactory.construct(response.content)
                )
        }
}

export default GetCustomerBrandWithIdFeature;
